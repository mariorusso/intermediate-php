CREATE TABLE IF NOT EXISTS customer (
  invoice_id bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  customer_id bigint(20) 
  email varchar(255) DEFAULT NULL,
  password varchar(255) DEFAULT NULL,
  first_name varchar(255) DEFAULT NULL,
  last_name varchar(255) DEFAULT NULL,
  street_1 varchar(255) DEFAULT NULL,
  street_2 varchar(255) DEFAULT NULL,
  city varchar(255) DEFAULT NULL,
  province varchar(255) DEFAULT NULL,
  postal_code varchar(255) DEFAULT NULL,
  phone varchar(255) DEFAULT NULL,
  created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at timestamp DEFAULT NULL,
  deleted tinyint(1) DEFAULT '0'
) 

ALTER TABLE customer CHANGE updated_at timestamp DEFAULT NULL
)