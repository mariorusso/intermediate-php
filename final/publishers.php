<?php
//include config
require '../inc/config.php';

//Set title variable
$title = 'Publishers';

// Assign function to a variable $dbh
$dbh = getPDO();

//PDO query Database
$query = $dbh->prepare("SELECT 
          publisher_id,
          name
          FROM
          publisher");
  
//Execute the query 
$query->execute();
$result = $query->fetchAll(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html>
<head>
	<title><?=$title?></title>
</head>
<body>

	<h1><?=$title?></h1>

	<h2>Please click a publisher to edit:</h2>

	<?php foreach($result as $row) : ?>
  
		<p><a href="edit_publisher.php?publisher_id=<?=$row['publisher_id']?>"><?=$row['name']?></a></p>
  
    <?php endforeach; ?>



</body>