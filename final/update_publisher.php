<?php
//include config
require '../inc/config.php';

//Set title variable
$title = 'Update Publisher';

$clean = array();

//Check to make sure have POST 
if($_SERVER['REQUEST_METHOD'] === 'POST'){
  
  //sanatize the post info
  $clean['publisher_id'] = intval($_POST['publisher_id']);
  $clean['name'] = sanatizeString($_POST['name']);
  $clean['phone'] = sanatizeString($_POST['phone']);
 
}
//If not post die.
else{
   die ('You don´t have a POST method.');
}//END Have POST

    
//Conect to Mysql
//$link = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)
//    or die(mysqli_connect_error());

// Assign function to a variable $dbh
$dbh = getPDO();

//Escape special characters so does not alter the MySQL and Define variables.
//$publisherid = $clean['publisher_id'];
//$name = mysqli_real_escape_string($link, $clean['name']); 
//$phone = mysqli_real_escape_string($link, $clean['phone']);

$publisher_id = $clean['publisher_id'];
$name = $clean['name'];
$phone = $clean['phone'];
  

//Create query string
$query = $dbh->prepare("UPDATE 
          publisher
          SET name=?, phone=?
          WHERE
          publisher_id=?");

// Execute query
//$result = mysqli_query($link, $query);

$params = array($name, $phone, $publisher_id);

//Execute the query 
$query->execute($params);
//$row = $query->fetch(PDO::FETCH_ASSOC);

?><!DOCTYPE html>
<html>
<head>
	<title><?=$title?></title>
</head>
<body>

	<h1><?=$title?></h1>

	
		<h2>Publisher successfully updated</h2>

		<p><strong>Name: </strong><?php echo htmlentities($name) ?><br />
		<strong>Phone: </strong><?php echo htmlentities($phone) ?></p>

		<p>Please <a href="publishers.php">click here</a> to edit another publisher.</p>
	
	


	


</body>