<?php
//include config
require '../inc/config.php';

//Set title variable
$title = 'Edit Publisher';

//Check for GET publisher_id or die
if(isset($_GET['publisher_id'])){
 
  //sanatize the $_GET info, make sure is a integer.
  $publisher_id = intval($_GET['publisher_id']);  
}

//If is not GET.
else {
    //Stop script and display a message. 
	die("<p>Please go back and select a <a href='publishers.php'>publisher</a> to edit.</p>");
}

/*/Conect to Mysql
$link = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME)
        or die(mysqli_connect_error());
*/
// Assign function to a variable $dbh connect by function.
$dbh = getPDO();

/*/Create query string
$query = "SELECT 
          publisher_id,
          name,
          phone
          FROM
          publisher
          WHERE
          publisher_id={$publisherid}";
*/
//PDO query Database
$query = $dbh->prepare("SELECT 
          publisher_id,
          name,
          phone
          FROM
          publisher
          WHERE
          publisher_id=?");

//Set parameters as an array getting book_id
$params = array($publisher_id);

// Execute query and get results
//$result = mysqli_query($link, $query);

// Extract single record as $row
//$row = mysqli_fetch_assoc($result);

//Execute the query 
$query->execute($params);
$row = $query->fetch(PDO::FETCH_ASSOC);

?><!DOCTYPE html>
<html>
<head>
	<title><?=$title?></title>
</head>
<body>

	<h1><?=$title?></h1>

	<form action="update_publisher.php" method="post">
	
		<input type="hidden" name="publisher_id" value="<?php echo $row{'publisher_id'} ?>" />
		<p><label>Publisher Name</label><br />
		<input type="text" name="name" value="<?php echo $row{'name'} ?>" /></p>
		<p><label>Publisher Phone</label><br />
		<input type="text" name="phone" value="<?php echo $row{'phone'} ?>" /></p>
		<p><input type="submit" value="Submit" /></p>

	</form>



</body>