<?php
/**
  file: user_list_1.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Jan 29 2015
  description: Intermediate PHP Final exam pratical part1
*/
try{
//Include the config file 
require '../inc/config.php';

//Define the title variable
$title = 'User List';

//Connect to the database using PDO 
$dbh = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_PASS);

//Query the data base to SELECT name and email FROM the user TABLE and postal code and phone FROM the address TABLE
$sql = "SELECT u.user_id, u.name, u.email, a.postal_code, a.phone FROM address a JOIN user u on a.user_id = u.user_id";

//Prepare the query 
$query = $dbh->prepare($sql);

//Execute the query and extract the results into an array. 
$query->execute();
$result = $query->fetchAll(PDO::FETCH_ASSOC);

}catch(Exception $e){
    $error = $e->getMessage();
}

?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>User List</title>
	<style>

	body {
		padding: 0px 50px 50px;
	}

	table {
		border-collapse: collapse;
	}

	td, th {
		padding: 10px;
		border: 1px solid #cfcfcf;
		background: #efefef;
	}


	</style>
</head>
	<body>
        <!--//display the title -->
		<h1><?=$title?></h1>

		<form action="user_list_2.php" method="get">
		<p><input type="text" name="q" />&nbsp;<input type="submit" value="Search" /></p>
		</form>

		<br /><hr /><br />

		<table>

		<thead>
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th>Postal Code</th>
			<th>Phone</th>
		</tr>

		</thead>

		<tbody>
 
            
          <!-- //loop thru the result array -->
          <?php foreach($result as $row) : ?>
         
            <!-- //display the results in a table. --> 
            <tr>
              <td><?=$row['name']?></td>
			  <td><?=$row['email']?></td>
			  <td><?=$row['postal_code']?></td>
			  <td><?=$row['phone']?></td>
		    </tr>
    
          
        <?php endforeach; ?>
        <!-- //End loop thru the result array -->

	
		</tbody>
		
		</table>


	</body>
</html>
