<?php
/**
  file: user_list_1.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Jan 29 2015
  description: Intermediate PHP Final exam pratical part1
*/

//START
  //Include the config file 

  //Define the title variable

  //Connect to the database using PDO 

  //Check if you have GET from search
  
    //If have a GET 

      //Sanatize the search term
    
      //Query the data base to SELECT name and email FROM the user TABLE and postal code and phone FROM the address TABLE
      //WHERE is LIKE the searchterm.

      //set the parameters from the search to the execution.

      //Execute the query and extract the results into an array. 
  
    // End of is Set GET.

  //Else
    //Query the data base to SELECT name and email FROM the user TABLE and postal code and phone FROM the address TABLE

    //Prepare the query 

    //Execute the query and extract the results into an array. 

?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>User List</title>
	<style>

	body {
		padding: 0px 50px 50px;
	}

	table {
		border-collapse: collapse;
	}

	td, th {
		padding: 10px;
		border: 1px solid #cfcfcf;
		background: #efefef;
	}


	</style>
</head>
	<body>
    
		<h1><?php //display the title  ?>User List</h1>

		<form action="user_list.php" method="get">
		<p><input type="text" name="q" />&nbsp;<input type="submit" value="Search" /></p>
		</form>

		<br /><hr /><br />

		<table>

		<thead>
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th>Postal Code</th>
			<th>Phone</th>
		</tr>

		</thead>

		<tbody>
<?php 

//loop thru the result array

//display the reuslt in a table. 

//end the loop

?>
		<tr>
			<td>Tom Jones</td>
			<td>tjone@hotmail.com</td>
			<td>R3N 0Y4</td>
			<td>204-222-9876</td>
		</tr>

		

	
		</tbody>
		
		</table>


	</body>
</html>
