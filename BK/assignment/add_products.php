<?php
/**
  file: manage_products.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Feb 01 2015
  description: Manage Products Page TCP   
*/
$title = 'ADD PRODUCTS';

require '../inc/proj_config.php';

$errors = false;

//check if have post.
if($_SERVER['REQUEST_METHOD'] == 'POST') {
  
  //ensure all fields were filled in or set error
  foreach($_POST as $key => $value){
    if(empty($value)){
        $errors[$key] = "$key is a required field";
      }
  }
 
  //If NO errors
  if(!$errors){
    
    //Assign the $_POST variable to normal variables. 
    $category_id = $_POST['category_id'];
    $name = $_POST['name'];
    $qty = $_POST['quantity'];
    $low_qty = $_POST['lowstock_qty']; 
    $short_desc = $_POST['short_description'];
    $long_desc = $_POST['long_description'];
    $image = $_POST['image_id'];
    $price = $_POST['price'];
    $cost = $_POST['cost'];
    //seting the checkbox
    if(isset($_POST['featured'])){
      //$featured is checked and value = 1
      $featured = $_POST['featured'];
      $featured = 1;
    }
    else{
      //$stok is nog checked and value=0
      $featured=0;
    }
   
    
    //conect to database
    $dbh = getPDO();
    
    //Query the database 
    $sql = "INSERT INTO products (category_id, name, quantity, lowstock_qty, short_description, long_description, image_id, price, cost, featured)
            VALUES (:cat_id, :name, :quantity, :lowstock_qty, :short_desc, :long_desc, :image, :price, :cost, :featured)";
    
    //Prepare the query to database.
    $query = $dbh->prepare($sql);
    
    //Set the parameters to be executed associating the prepared statement 
    //to the the variables with the values that we got from the form POST.
    $params = array(':cat_id'=>$category_id, ':name'=>$name, ':quantity'=>$qty, ':lowstock_qty'=>$low_qty, ':short_desc'=>$short_desc,                        ':long_desc'=>$long_desc, ':image'=>$image, ':price'=>$price, ':cost'=>$cost, ':featured'=>$featured);
    
    //Execute the query.
    $query->execute($params);
    
    /* SELECT THE NEW PRODUCT FOR DISPLAY
    --------------------------------------------------------------------------------------*/
    $prod_id = $dbh->lastInsertId();
    
    $query = $dbh->prepare('SELECT * FROM products WHERE prod_id = ?');
    
    $params = array($prod_id);
    
    $query->execute($params);
    
    $product = $query->fetch(PDO::FETCH_ASSOC);
    
    
  }//End of if not errors
}// End of have $_post

include '../inc/adm_header_inc.php';

?>
        
    <div id="breadcrumbs">
      <p><?=$title?></p>
    </div>
    <!-- Start of Content --> 
    <div id="content_wrapper"> 
        
        <!--Tob bar of the column-->
        <div class="column_top">
            <h1><?=$title?></h1>
          </div>
        
        <?php include '../inc/adm_sidebar_inc.php'; ?>
        
        <div id="main_content">  
          <?php if($errors) : ?>
            
            <div class="erros">
          
              <?php foreach($errors as $key => $value) : ?>
            
                <p><?=prettyString($value)?></p>
        
              <?php endforeach; ?>
        
            </div>
          <?php endif; ?>
    
        <?php if(isset($product)) : ?>
          <?php foreach($product as $key => $value) : ?>
            <p><?=$key?> - <?=$value?></p>
          <?php endforeach; ?>
        <?php else : ?>
    
          <?=formOpen('post', '#' , 'insert_form')?>
    
            <?=createTextInput('name', 100)?>
    
            <?=getCategory('category_id')?>
    
            <?=createTextInput('quantity', 100)?>
    
            <?=createTextInput('lowstock_qty', 100)?>
    
            <?=createTextArea('short_description')?>
          
            <?=createTextArea('long_description')?>
    
            <?=createTextInput('image_id', 100)?>
      
            <?=createTextInput('price', 100)?>
          
            <?=createTextInput('cost', 100)?>
          
            <?=createCheckbox('featured')?>
    
            <?=createSubmit()?>
    
          <?=formClose()?>
    
        <?php endif; ?>
        
      </div>  
    </div>
      <!-- End of Content -->
      
 <?php 
 
 include '../inc/adm_footer_inc.php';
 
 ?>