<?php
/**
  file: dashboard.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Feb 01 2015
  description: Administrative Dashboard.   
*/

$title = 'DASHBOARD';

require '../inc/proj_config.php';

include '../inc/adm_header_inc.php';

?>
        
      <div id="breadcrumbs">
        <p><?=$title?></p>
      </div>
      <!-- Start of Content --> 
      <div id="content_wrapper"> 
        
        <!--Tob bar of the column-->
        <div class="column_top">
            <h1><?=$title?></h1>
          </div>
        
        
        
      </div>
      <!-- End of Content -->
      
 <?php 
 
 include '../inc/adm_footer_inc.php';
 
 ?>