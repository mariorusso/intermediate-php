<?php
/**
  file: regex.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Jan 22 2015
  description: Regular expression  
*/

require '../inc/config.php';

$string = '204-330-3249';
// phone /[\d]?[\.\ \-]?([\d]{3})[\.\ \-]?([\d]{3})[\.\ \-]?([\d]{4})/

// canadian postal code ([a-z]{1}[\d]{1}[a-z]{1})[\s\.\-]?([\d]{1}[a-z]{1}[\d]{1})

// canadian region [A-Z]{2}



// password (?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[\!\@\#\$\%\&\^\~\(\)\-])(.{8,})
/*
(?=.*[0-9]) -> look ahead for a number
(?=.*[A-Z]) -> look ahead for an uppercase letter
(?=.*[a-z]) -> look ahead for a lowercase letter
(?=.*[\!\@\#\$\%\&\^\~\(\)\-]) -> look ahead for a symbol
(.{10,}) -> make sure we have 10 character minimum total
*/
