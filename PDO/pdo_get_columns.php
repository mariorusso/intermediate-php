<?php
/**
  file: pdo_get_columns.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Jan 23 2015
  description: PDO Query  
*/

require '../inc/config.php';

$title = "PDO Pretty Output";

// Assign function to a variable $dbh
$dbh = getPDO();

//PDO query Database
$query = $dbh->prepare("SELECT title, author, num_pages, year_published, publisher, genre, format, in_print FROM catalog");

//Execute the query 
$query->execute();
$result = $query->fetchAll(PDO::FETCH_ASSOC);

//Get columns
$columns = getColumns($result);

?><!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
    <style>
      
      h1{
        text-align: center; 
        color: #ddd;
        text-shadow: 0px 1px 1px rgba(0,0,0,0.6);
      }
      
      a{
        font-weight: bold;
        color: #000;
        text-decoration: none;
      }
      
      a:hover{
        font-weight: bold;
        color: #f00;
        text-decoration: none;
      }
      
      body{
       font-family: Arial, Helvetica, sans-serif;
       font-size: 16px; 
      }
      
      table{
        border-collapse: collapse;
        margin: 0 auto;
      }
      
      table td, th{
        border: solid #000 2px;
        padding: 8px;
        background-color: #ccc;
      }
      
      table th{
        background-color: #fff; 
      }
      
    </style>
  </head> 
  <body>
      
    <h1><?=$title?></h1>
        
    <table>
      <tr>
        <?php foreach($columns as $value) : ?>
            <th><?=$value?></th>
        <?php endforeach;  ?>
      </tr>
      
      <?php foreach($result as $row) : ?>
        
        <tr>
          <?php foreach($row as $value) : ?>
            <td><?=$value?></td>
          <?php endforeach ?>          
        </tr>
        
      
      <?php endforeach; ?>
      
    
    </table>
    
  </body>
</html>