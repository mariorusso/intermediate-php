<?php
/**
  file: PDOquery.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Jan 23 2015
  description: PDO Query  
*/

require '../inc/config.php';

$title = "PDO Query";

// Assign function to a variable $dbh
$dbh = getPDO();

//PDO query Database
$query = $dbh->prepare("SELECT * FROM catalog WHERE book_id=4");

//Execute the query 
$query->execute();
$row = $query->fetch(PDO::FETCH_ASSOC);


?><!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
  </head> 
  <body>
      
    <h1><?=$title?></h1>
        
    <pre>
      
    <?php
  
       print_r($row);
       
      
      ?>
    </pre>

    
  </body>
</html>