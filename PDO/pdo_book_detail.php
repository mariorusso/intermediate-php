<?php
/**
  file: pdo_book_detail.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Jan 23 2015
  description: PDO Book Detail  
*/

require '../inc/config.php';

$title = "PDO Book Detail";

if(isset($_GET['book_id'])){
  $book_id = intval($_GET['book_id']);
}
else{
  die('no get');
}
                    
// Assign function to a variable $dbh
$dbh = getPDO();

//PDO query Database passing parameter '?'
$query = $dbh->prepare("SELECT * FROM book WHERE book_id=?");

//Set parameters as an array getting book_id
$params = array($book_id);

//Execute the query 
$query->execute($params);
$row = $query->fetch(PDO::FETCH_ASSOC);


?><!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
  </head> 
  <body>
      
    <h1><?=$title?></h1>
        
    <pre>
      
    <?php
  
       print_r($row);
       
      
      ?>
    </pre>

    
  </body>
</html>