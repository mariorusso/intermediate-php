<?php
/**
  file: PDOquery.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Jan 23 2015
  description: PDO Query  
*/

require '../inc/config.php';

$title = "PDO Query";

// Assign function to a variable $dbh
$dbh = getPDO();

//PDO query Database
$query = $dbh->prepare("SELECT * FROM catalog");

//Execute the query 
$query->execute();
$result = $query->fetchAll(PDO::FETCH_ASSOC);


?><!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
  </head> 
  <body>
      
    <h1><?=$title?></h1>
        
    <pre>
      
    <?php foreach($result as $row){
  
       print_r($row);
        echo '<hr/>';
      }
      ?>
    </pre>

    
  </body>
</html>