<?php
/**
  file: pdo_get_columns.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Jan 23 2015
  description: Search results using PDO  
*/

require '../inc/config.php';

$title = "PDO Search Results";
if(isset($_GET['searchterm'])){
  $searchterm = sanatizeString($_GET['searchterm']);
}
else {
  $searchterm = '';
}


// Assign function to a variable $dbh
$dbh = getPDO();

//PDO query Database
$query = $dbh->prepare("SELECT book_id,
                               title, 
                               author,
                               num_pages,
                               year_published,
                               publisher,
                               genre, 
                               format, 
                               in_print
                        FROM catalog
                        WHERE title LIKE ?
                        OR
                              author LIKE ?");

$params = array("%$searchterm%", "%$searchterm%");

//Execute the query 
$query->execute($params);
$result = $query->fetchAll(PDO::FETCH_ASSOC);

if($result != true){
  echo '<a href="pdo_search.php"><< Back to search</a> No Results Match your search!';
  die;
}

//Get columns
$columns = getColumns($result);

?><!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
    <style>
      
      h1{
        text-align: center; 
        color: #ddd;
        text-shadow: 0px 1px 1px rgba(0,0,0,0.6);
      }
      
      a{
        font-weight: bold;
        color: #000;
        text-decoration: none;
      }
      
      a:hover{
        font-weight: bold;
        color: #f00;
        text-decoration: none;
      }
      
      body{
       font-family: Arial, Helvetica, sans-serif;
       font-size: 16px; 
      }
      
      table{
        border-collapse: collapse;
        margin: 0 auto;
      }
      
      table td, th{
        border: solid #000 2px;
        padding: 8px;
        background-color: #ccc;
      }
      
      table th{
        background-color: #fff; 
      }
      
    </style>
  </head> 
  <body>
      
    <h1><?=$title?></h1>
    <p><a href="pdo_search.php"><< Back to search</a></p>    
    <table>
      <tr>
        <?php foreach($columns as $value) : ?>
            <th><?=$value?></th>
        <?php endforeach;  ?>
      </tr>
      
      <?php foreach($result as $row) : ?>
        
        <tr>
          <?php foreach($row as $key => $value) : ?>
            
            <?php if($key == 'title') : ?>
            
              <td>
                
                <a href="pdo_book_detail.php?book_id=<?=$row['book_id']?>"><?=$value?></a></td>
            
            <?php else : ?>
              <td><?=$value?></td>
            
            <?php endif; ?>
                
          <?php endforeach ?>          
        </tr>
        
      
      <?php endforeach; ?>
      
    
    </table>
    
  </body>
</html>