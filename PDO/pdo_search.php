<?php
/**
  file: pdo_search.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: 2015-01-12
  description: PDO Search Box
*/

ini_set('display_errors',1);// display erros
ini_set('error_reporting',E_ALL);// show all errors

$title = "PDO Search";

?><!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
  </head> 
  <body>
      
    <h1><?=$title?></h1>
    
     <form 
        action="pdo_search_results.php"
        method="get"
     >
       <p>
         <label for="search">Search:</label>
         <input type="text" name="searchterm" />
         
         <input type="submit" value="Send" />
           
      </p>
       
    </form>
   
  </body>
</html>