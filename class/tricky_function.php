<?php
/**
  file: template.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Jan 22 2015
  description:Template  
*/

require '../inc/config.php';

function trickyFunction(&$x){
  $x = $x + $x;
  return $x;
}

$num = 6;

$title = "Template";


?><!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
  </head> 
  <body>
      
    <h1><?=$title?></h1>
    <p><?=$num?></p>
    <p><?=trickyFunction($num)?></p>
    <p><?=$num?></p>
    
  </body>
</html>