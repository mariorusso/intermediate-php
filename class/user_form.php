<?php
/**
  file: my_form.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: 2015-01-12
  description: contact form  
*/

ini_set('display_errors',1);// display erros
ini_set('error_reporting',E_ALL);// show all errors

require '../inc/config.php';

require '../inc/functions.php';

$title = "Contact Form";


?><!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
  </head> 
  <body>
      
    <h1><?=$title?></h1>
    
<?php
  $errors = false;       
  
     if($_SERVER['REQUEST_METHOD'] == 'POST') {
    
       
        $errors['first_name'] = validateString($_POST['first_name'], 'First Name');
        $errors['last_name'] = validateString($_POST['last_name'], 'Last Name');
        $errors['email_address'] = validateString($_POST['email_address'], 'Email Address');
        $errors['age'] = validateInteger($_POST['age'], 18, 99, 'Age');
        
        $clean = array();
        foreach($_POST as $key => $value){
          
          $clean[$key] = sanatizeString($value);
        
        }
     }
     
?>
    
     <form 
        action="#"
        method="post"
      >
       <p>
         <label for="first_name">First Name:</label>
         <input type="text" name="first_name" value="<?php
            if(isset($clean['first_name'])) {
             echo $clean['first_name']; 
           }
         ?>" />&nbsp;
         <?php
         if(!empty($errors['first_name'])) {
           echo $errors['first_name']; 
         }
         ?>
       </p>
      
       <p>
         <label for="last_name">Last Name:</label>
         <input type="text" name="last_name" value="<?php
            if(isset($clean['last_name'])) {
             echo $clean['last_name']; 
           }
         ?>" />&nbsp;
         <?php
         if(!empty($errors)) {
           echo $errors['last_name']; 
         }
         ?>
       </p>
       
       <p>
         <label for="email_address">email:</label>
         <input type="text" name="email_address" value="<?php
            if(isset($clean['email_address'])) {
             echo $clean['email_address']; 
           }
         ?>" />&nbsp;
         <?php
         if(!empty($errors)) {
           echo $errors['email_address']; 
         }
         ?>
       </p> 
       
       <p>
         <label for="age">age:</label>
         <input type="text" name="age" value="<?php
            if(isset($clean['age'])) {
             echo $clean['age']; 
           }
         ?>" />&nbsp;
         <?php
         if(!empty($errors)) {
           echo $errors['age']; 
         }
         ?>
       </p>
       
       <p>
         <input type="submit" value="Send" />
           
      </p>
       
    </form>
<pre>   
<?php
     if($_SERVER['REQUEST_METHOD'] == 'POST') {
      
       if(!$errors) {
         print_r($_POST); 
       }
     }
 
?>
</pre>   
  </body>
</html>