<?php
/**
  file: crypt_p.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Jan 27 2015
  description: Login form   
*/

require '../inc/config.php';

require '../inc/functions.php';

$title = "Login Form";

//encrypted password stored in database
$stored_encrypted = '$2y$10$54c7cbc5019995.849676uspqyOizQ/nbFhkZKFs30DJX4o2C.VYe';
$stored_email = 'mario@gmail.com';
$logged_in = false;
$message = ' ';


if($_SERVER['REQUEST_METHOD'] == 'POST'){
  
  //submited by user in $_POST
  $login_password = $_POST['password'];
  $login_email = $_POST['email'];

  $submitted_encrypted = crypt($login_password, $stored_encrypted);

  if($stored_encrypted == $submitted_encrypted && $login_email == $stored_email) {
    $logged_in = true;
    $message = 'You are logged in!';
  } else {
    $logged_in = false;
    $message = 'Login failed!';
  }
}
?><!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
  </head> 
  <body>
      
    <h1><?=$title?></h1>
    <?php if($logged_in == false) : ?>
      <h2><?=$message?></h2>
    <?=formOpen()?>
    
      <?=createTextInput('email', 100)?>
    
      <?=createPasswordInput('password', 30)?>
    
      <?=createSubmit()?>
    
    <?=formClose()?>
    
    <?php else : ?>
      <h2><?=$message?></h2>
    
    <?php endif; ?>
    
    
  </body>
</html>