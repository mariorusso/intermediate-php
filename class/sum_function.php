<?php
/**
  file: sum_function.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Jan 26 2015
  description: Sum function
*/

require '../inc/config.php';

$num1 = 3;
$num2 = 2;
$num3 = 4;
$num4 = 5;

function getSum($num1, $num2, $num3, $num4){
  
  $result = $num1 + $num2 + $num3 + $num4;
  
  return $result;
}

$title = "Sum function";


?><!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
  </head> 
  <body>
      
    <h1><?=$title?></h1>
    
    <?=getSum(2, 7, 10, 5)?>
    
  </body>
</html>