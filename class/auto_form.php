<?php
/**
  file: template.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Jan 22 2015
  description:Template  
*/

require '../inc/config.php';

$title = "Auto form";


?><!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
  </head> 
  <body>
      
    <h1><?=$title?></h1>
  
      <?=formOpen()?>
    
      <?=createTextInput('first_name', 255 )?>
      
      <?=createTextInput('last_name', 255)?>
    
      <?=createTextInput('city', 64)?>
      
      <?=getRegion('province')?>
      
      <?=createTextInput('email', 255)?>
      
      <?=createTextInput('phone', 10)?>
       
      <?=createSubmit()?>
    
      <?=formClose()?>
    
  </body>
</html>