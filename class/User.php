<?php
/**
  file: template.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Jan 22 2015
  description:Template  
*/

require '..inc/config.php';

$title = "Template";

class User{
  
  private $first_name;
  private $last_name;
  private $email;
  
  public function setFirstName($name) {
    $this->first_name = $name;
  }
  
  public function setLastName($name) {
    $this->last_name = $name;
  }
  
  public function setEmail($name) {
    $this->email = $name;
  }
    
}

$fred = new User();

$fred->setFirstName('Fred');
$fred->setLastName('Flinstone');
$fred->setEmail('fred@aol.com');

?><!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
  </head> 
  <body>
      
    <h1><?=$title?></h1>
    
  </body>
</html>