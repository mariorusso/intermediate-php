<?php
/**
  file: customer_list.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Jan 28 2015
  description: Customer List 
*/

require '../inc/proj_config.php';

$title = "Customer List";

//conect to database
$dbh = getPDO();
    
//Query the database 
$sql = "SELECT customer_id, first_name FROM customer";
    
//Prepare the query to database.
$query = $dbh->prepare($sql);
    
//Execute the query.
$query->execute();
$result = $query->fetchAll(PDO::FETCH_ASSOC);

?><!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
  </head> 
  <body>
      
    <h1><?=$title?></h1>
    
      <div class="list">
                  
            <?php foreach($result as $row) : ?>
              
              <p><a href="update_form.php?customer_id=<?=$row['customer_id']?>"><?=$row['first_name']?></a></p>
                              
            <?php endforeach; ?>
               
      </div>
    
  </body>
</html>