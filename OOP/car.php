<?php
/**
  file: car_class.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Jan 26 2015
  description: Using OOP Creating classes
*/

require '../inc/config.php';

$title = "OOP Car Class";

class Car extends Vehicle {
  
  private $model = null;
  private $year = null;
  private $color = null;
  private $price = null;
  
  public function __construct($model, $color, $price, $year) {
   
    $this->model = $model;
    $this->color = $color;
    $this->price = $price;
    $this->year = $year;
  }
  
  public function setPrice($price) {
    $this->price = $price; 
  }
  
  public function setModel($model) {
    $this->model = $model;
  }
  
  public function setYear($year) {
    $this->year = $year;
  }
}// End car


//Test CODE
$chevy = new Car('Impala', '2012', 'Royal Blue', '250000');
$honda = new Car('Civic', '2014', 'Red', '12000');
 


?><!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
  </head> 
  <body>
      
    <h1><?=$title?></h1>
    
    <?php 
      
      echo $chevy->model;
      echo '<br />';
      echo $chevy->year;
      echo '<br />';
      echo $chevy->color;
      echo '<br />';
      echo $chevy->price;
      echo '<br /><br />';

      echo $honda->model;
      echo '<br />';
      echo $honda->year;
      echo '<br />';
      echo $honda->color;
      echo '<br />';
      echo $honda->price;
  
      ?>
     
  </body>
</html>