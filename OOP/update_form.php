<?php
/**
  file: update_form.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Jan 28 2015
  description: Update form   
*/

require '../inc/proj_config.php';

$title = "Update Customer Form";

$errors = false;

//check if have get.
if(isset($_GET['customer_id'])) {
  
    //conect to database
    $dbh = getPDO();
    
    //Query the database 
    $sql = "SELECT email, password, first_name, last_name, street_1, street_2, city, province, postal_code, phone FROM customer WHERE                         customer_id=?";
    
    //Prepare the query to database.
    $query = $dbh->prepare($sql);
    
    $params = array($_GET['customer_id']);
    
    //Execute the query.
    $query->execute($params);
    $result = $query->fetch(PDO::FETCH_ASSOC);
  
}else{
   die ('here');
}// End of have $_GET

?><!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
  </head> 
  <body>
      
    <h1><?=$title?></h1>
    <?php if($errors) : ?>
      <div class="erros">
        <?php foreach($errors as $key => $value) : ?>
            
           <p><?=prettyString($value)?></p>
        
        <?php endforeach; ?>
        
      </div>
    <?php endif; ?>
    
    <?=formOpen()?>
    
      <?=createTextInput('first_name', 100)?>
    
      <?=createTextInput('last_name', 100)?>
    
      <?=createTextInput('email', 100)?>
    
      <?=createTextInput('phone', 12)?>
    
      <?=createTextInput('street_1', 100)?>
    
      <?=createTextInput('street_2', 100)?>
    
      <?=createTextInput('city', 100)?>
    
      <?=getRegion('province')?>
    
      <?=createTextInput('postal_code', 7)?>
    
      <?=createPasswordInput('password', 30)?>
        
      <?=createPasswordInput('confirm_password', 30)?>
    
      <?=createSubmit()?>
    
    <?=formClose()?>
    
    
    
    
  </body>
</html>