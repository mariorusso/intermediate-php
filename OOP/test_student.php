<?php
/**
  file: test_person.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Jan 26 2015
  description: test person
*/

require '../inc/config.php';


$title = "Test Student";

?><!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
  </head> 
  <body>
      
    <h1><?=$title?></h1>
    <pre>
    <?php 
      $steve = new Student('Steve', 19, '1234', 'Uwinnipeg');
        
      print_r($steve);

      $steve->ageOneYear();
      
      print_r($steve);

      echo $steve->getName();

      echo '<br />';
        
      echo $steve->getAge();
     
      ?>
     </pre>
  </body>
</html>