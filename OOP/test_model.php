<?php
/**
  file: test_model.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Jan 26 2015
  description: test Model
*/

require '../inc/config.php';


$title = "Test Model";

?><!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
  </head> 
  <body>
      
    <h1><?=$title?></h1>
   
    <?php 
      
      $model = new Model(DB_HOST, DB_NAME, DB_USER, DB_PASS);

      print_r($model);
      
      ?>
    
  </body>
</html>