<?php
/**
  file: test_person.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Jan 26 2015
  description: test person
*/

require '../inc/config.php';

include 'Person.php';

$title = "Test Person";

?><!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
  </head> 
  <body>
      
    <h1><?=$title?></h1>
    <pre>
    <?php 
      $steve = new Person('Steve', 19);
        
      print_r($steve);

      $steve->ageOneYear();
      
      print_r($steve);
     
      ?>
     </pre>
  </body>
</html>