<?php
/**
  file: test_validator.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Jan 26 2015
  description: test validator
*/

require '../inc/config.php';


$title = "Test Validator";

?><!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
  </head> 
  <body>
      
    <h1><?=$title?></h1>
   
    <?php 
      $mystring = '   <b>Hi, There!</b>   ';
      
      echo '=>' . $mystring . '<=';

      $clean_string = Validator::sanitizeString($mystring);

      echo '<br/>';

      echo '=>' . $clean_string . '<=';
      ?>
    
  </body>
</html>