<?php
/**
  file: string_functions.php
  author: Mario Russo <mariorusso@gmail.com>
  Updated: Jan 27 2015
  description: String Functions
*/

require '../inc/config.php';


$title = "String Functions";

$string = " my car is green  ";
  
$string1 = "Winnipeg";

$string2 = 'Winters in Winnipeg are very cold and I like Winnipeg';

$string3 = substr($string2, 11, 8);

$string4 = 'I live in Winnipeg 
            here is very cold,      
            because is in north america.';

$string5 = nl2br($string4);

$string6 = "oh give me a home where the buffalo rowm";

$string7 = explode(' ', $string6);

$string8 = implode(' ', $string7);

$plain_pass = 'password';

$salt = time();

$ecrypted = md5($plain_pass . $salt);

?><!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
  </head> 
  <body>
      
    <h1><?=$title?></h1>

    <?php
     
      echo "<p>$string</p>";

      echo '<p>' . trim($string) . '</p>';
      
      echo '<p> string lentgh: ' . strlen($string1) . '</p>';

      echo '<p> string position: ' . strpos($string2, 'Winnipeg') . '</p>';
      
      echo '<p> string reverse position: ' . strrpos($string2, 'Winnipeg') . '</p>';

      echo '<p> using substr: ' . $string3 . '</p>';

      echo '<p> multiline: ' . $string4 . '</p>';
      
      echo '<p> multiline with nl2br: ' . $string5 . '</p>';
      
      echo '<pre>';
      print_r($string7);
      echo '</pre>';
    
      echo '<p>using implode from array: ' . $string8 . '</p>';
      
      echo '<p> using ucword: ' . ucwords($string8) . '</p>';
      
      echo '<p> using ucfirst: ' . ucfirst($string8) . '</p>';

      echo '<p> using strtolower: ' . strtolower($string8) . '</p>';
      
      echo '<p> using strtoupper: ' . strtoupper($string8) . '</p>';

      echo '<p> using md5 : ' . md5($plain_pass) . '</p>';

      echo '<p> using md5 + salt : ' . md5($plain_pass . $salt) . '</p>';

    
    ?>
     
    
  </body>
</html>