<?php
/**
  file: login_form.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Jan 27 2015
  description: Login form   
*/

require '../inc/proj_config.php';

$title = "Login Form";

$errors = false;

//check if have post.
if($_SERVER['REQUEST_METHOD'] == 'POST') {
  
  //ensure all fields were filled in or set error
  foreach($_POST as $key => $value){
   if(empty($value)){
    $errors[$key] = "$key is a required field";
   }
  }
  
  //Ensure paswword field == confirm password field or set error
  if($_POST['password'] !== $_POST['confirm_password']){
    $errors['password'] = "Passwords does not match!";
  }   
  
  //If NO errors
  if(!$errors){
    
    //Assign the $_POST variable to normal variables. 
    $firstname = $_POST['first_name'];
    $lastname = $_POST['last_name'];
    $email = $_POST['email'];
        
    //encrypt the password. 
    $salt = uniqid('$2y$10$', true);
    $encrypted_password = crypt($_POST['password'], $salt);
    
    //conect to database
    $dbh = getPDO();
    
    //Query the database 
    $sql = "INSERT INTO customer ( first_name, last_name, email, password )
            VALUES (?, ?, ?, ?)";
    
    //Prepare the query to database.
    $query = $dbh->prepare($sql);
    
    //Set the parameters to be executed associating the prepared statement 
    //to the the variables with the values that we got from the form POST.
    $params = array( $firstname, $lastname, $email, $encrypted_password);
    
    //Execute the query.
    $query->execute($params);
    
  }//End of if not errors
}// End of have $_post

?><!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
  </head> 
  <body>
      
    <h1><?=$title?></h1>
    <?php if($errors) : ?>
      <div class="erros">
        <?php foreach($errors as $key => $value) : ?>
            
           <p><?=prettyString($value)?></p>
        
        <?php endforeach; ?>
        
      </div>
    <?php endif; ?>
    
    <?=formOpen()?>
    
      <?=createTextInput('first_name', 100)?>
    
      <?=createTextInput('last_name', 100)?>
    
      <?=createTextInput('email', 100)?>
    
      <?=createPasswordInput('password', 30)?>
        
      <?=createPasswordInput('confirm_password', 30)?>
    
      <?=createSubmit()?>
    
    <?=formClose()?>
    
    
    
    
  </body>
</html>