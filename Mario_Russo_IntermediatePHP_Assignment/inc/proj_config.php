<?php
session_start();

ini_set('display_errors',1);// display erros
ini_set('error_reporting',E_ALL);// show all errors

if(!isset($_SESSION['xsrf_token'])){
  $_SESSION['xsrf_token'] = md5(time());
}

define('DB_HOST','localhost');
define('DB_USER','mr_php');
define('DB_NAME','intro_php');
define('DB_PASS','mr1984');

require '../inc/functions.php';

function __autoload($class_name){
  include '../inc/classes/' . $class_name . '.php';
}