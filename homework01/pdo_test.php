<?php
/**
  file: pdo_connect.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: 2015-01-22
  description:Test connectio using pdo
*/
try{
  require_once '../inc/pdo_connect.php';
} catch(Exception $e){
    $error = $e->getMessage();
}

ini_set('display_errors',1);// display erros
ini_set('error_reporting',E_ALL);// show all errors

$title = "Pdo connection test!";


?><!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset="utf-8" />
    <title><?=$title?></title>
  </head> 
  <body>
      
    <h1><?=$title?></h1>
    
    <?php 
      if($db){
        echo "<p>Connection Success!</p>";
      }elseif(isset($error)){
        echo "<p>$error</p>";        
      }
    ?>
  </body>
</html>