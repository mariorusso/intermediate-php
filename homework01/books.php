<?php
/**
  file: books.php
  author: Mario Russo <mariorusso@gmail.com>
  updated: Jan 22 2015
  description: Book List
*/

try{
 require_once '../inc/config.php';
 
 
//Query 	
 $sql = 'SELECT
 			b.book_id,
            b.title,
			b.author_id,
            a.name as author,
			b.publisher_id,
			p.name as publisher,
			b.genre_id,
            g.name as genre,
			b.format_id,
            f.name as format
         FROM 
            book b
            JOIN author a on b.author_id = a.author_id
			JOIN publisher p on b.publisher_id = p.publisher_id
            JOIN genre g on b.genre_id = g.genre_id
            JOIN format f on b.format_id = f.format_id';
	
  $dbh = getPDO();
  
  $result = $dbh->query($sql); 
}

catch(Exception $e){
    $error = $e->getMessage();
}



$title = "Book List";

$subtitle = "Click any item to view detail";


?><!DOCTYPE html>
<html>
<head>
	<title><?=$title?></title>
	<style>
		h1{
        color: #ddd;
        text-shadow: 0px 1px 1px rgba(0,0,0,0.6);
        }

		table {
			border-collapse: collapse;
		}
		th, td {
			padding: 0px;
			text-align: left;
			border: 1px solid #cfcfcf;
		}

		th {
			padding: 12px;
		}

		td a {
			display: block;
			padding: 12px;
		}

	</style>
</head>
<body>

	<h1><?=$title?></h1>

    <h2><?=$subtitle?></h2>

		<table>
			
			<tr>
				<th>Title</th>
				<th>Author</th>
				<th>Publisher</th>
				<th>Genre</th>
				<th>Format</th>
			</tr>
			
			<?php foreach($result as $row) { ?>
			
			<tr>
				<td><a href="book_detail.php?book_id=<?php echo $row['book_id']; ?>"><?php echo $row['title']; ?></a></td>
				<td><a href="author_detail.php?author_id=<?php echo $row['author_id']; ?>"><?php echo $row['author']; ?></a></td>
				<td><a href="publisher_detail.php?publisher_id=<?php echo $row['publisher_id']; ?>"><?php echo $row['publisher']; ?></a></td>
				<td><a href="genre_detail.php?genre_id=<?php echo $row['genre_id']; ?>"><?php echo $row['genre']; ?></a></td>
				<td><a href="format_detail.php?format_id=<?php echo $row['format_id']; ?>"><?php echo $row['format']; ?></a></td>
			</tr>
			
			<?php } ?>

						
		</table>



</body>
