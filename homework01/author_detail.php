<?php
/**
file: author_details.php
date Jan 22 2015
title: Author Details
**/

$title = 'Author Details';
try{

	//Contains your MYsql conect info;
	require_once '../inc/config.php';
    
	if(isset($_GET['author_id'])){
  		$author_id = intval($_GET['author_id']);
	}
	else{
  		die('Please provide a book_id');
	}


	// Query. 
	$sql = "SELECT
				*
          	  FROM 
          	  author
              WHERE
              author_id={$author_id}";

	$dbh = getPDO();
  
    $result = $dbh->query($sql); 
}
catch(Exception $e){
    $error = $e->getMessage();
}

?><!DOCTYPE html>
<html lang="en">
  <head> 
    <title><?=$title?></title>
    <meta charset="utf-8" />
    <style>
      
      body{
       font-family: Arial, Helvetica, sans-serif;
       font-size: 16px; 
      }
      
      h1{
        color: #ddd;
        text-shadow: 0px 1px 1px rgba(0,0,0,0.6);
      }
      
      a{
        font-weight: bold;
        color: #f00;
        text-decoration: none;
      }
      
      a:hover{
        font-weight: bold;
        color: #f00;
        text-decoration: underline;
      }
      
      table{
        margin: 0 auto;
        border-collapse: collapse;
      }
      
      table td, th{
        border: solid #000 2px;
        padding: 8px;
        width: 600px;
        background-color: #ccc;
      }
      
      table th{
        background-color: #fff;
        width: 200px;
      }
      
    </style>
  </head>
  
  <body>
    <h1><?=$title?></h1>
    <a href="books.php" ><< Back to books</a>
    
      <?php while($row = $result->fetch(PDO::FETCH_ASSOC)) { ?>
        
	  	<pre>   
		
<?php print_r($row); ?>
	  	
		</pre>
       
	<?php } ?>
    </table>
  </body>
</html>